//Node.js Introduction 

//User the "require" directive load Node.js modules
//The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol(HTTP)
//HTTP is a protocol that allows the fetching of resources such as HTML documents.
// The message sent by the "client", usually a Web browser, called "request"
//The message sent by the "server", as an answers are called "responses".

let http = require("http");

//Using this module's createServer() method, we can create an HTTP server that listen to request on a specified port and givers responses back to client.
//A port us a virtual point where network connections start and end 
/*http.createServer(function (request, response) {
	
	//Use the writeHead() method to:
	//Set a status code for the response - a 200 means ok
	//Set the content-type of  the reposnse as a plain text message
	response.writeHead(200, {'Content-Type' : 'text/plain'});

	//send the response with the content:
	response.end("Welcome to my page");

	//server will be assigned to port 4000 via "listen(4000)" method where server will listen to any request that are sent to it eventually communicating with our server.
}).listen(4000);

//when server is running, console will print the message:
console.log("Server is running at localhost:4000");*/

const server = http.createServer((request, response) => {


	//Accessing the "greeting" route return a message "Hello BPI"
	//"request" is object that is sent via the client (browser)
	//the "url" property refers to the url or link in the browser. 

	if (request.url == '/greeting'){

		response.writeHead(200,{'Content-Type': 'text/plain'});

		response.end("Welcome to BPI");
	} else if ( request.url == '/Customer-service') {

		response.writeHead(200,{'Content-Type': 'text/plain'});

		response.end("Welcome to Customer-service of your BPI");

	} else {

		//Set a status code for the response - a 404 means not found
		response.writeHead(404, {'Content-Type' : 'text/plain'});

		response.end("Page is not available");
	}

}).listen(4000);

console.log('Server running at localhost:4000');